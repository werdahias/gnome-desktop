<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<!--
     The GNU Public License 2 in DocBook
     Markup by Eric Baudais <baudais@okstate.edu>
     Maintained by the GNOME Documentation Project
     http://developer.gnome.org/projects/gdp
     Version: 1.0.1
     Last Modified: Dec 16, 2000
-->
<article id="index" lang="gl">
  <articleinfo>
    <title>Licenza Pública Xeral de GNU</title>    
    <copyright><year>2000</year> <holder>Free Software Foundation, Inc.</holder></copyright>

      <author><surname>Free Software Foundation</surname></author>

      <publisher role="maintainer">
        <publishername>Proxecto de documentación de GNOME</publishername>
      </publisher>

      <revhistory>
        <revision><revnumber>2</revnumber> <date>1991-06</date></revision>
      </revhistory>

    <legalnotice id="legalnotice">
      <para><address>Free Software Foundation, Inc. 
	  <street>51 Franklin Street, Fifth Floor</street>, 
	  <city>Boston</city>, 
	  <state>MA</state> <postcode>02110-1301</postcode>
	  <country>USA</country>
	</address>.</para>

      <para>Permítese a copia e distribución de copias textuais desta licenza, sempre que non se introduza ningunha modificación.</para>
    </legalnotice>

    <releaseinfo>Versión 2, Xuño de 1991</releaseinfo>

    <abstract role="description"><para>A maioría das licenzas de software están concibidas para privar o usuario da súa liberdade de compartir e modificar ese software.  Pola contra, a licenza pública xeral de GNU busca garantir a súa liberdade para compartir e modificar software libre, para se asegurar de que o software é libre para todos os seus usuarios.</para></abstract>
  
    <othercredit class="translator">
      <personname>
        <firstname>Fran Dieguez</firstname>
      </personname>
      <email>frandieguez@gnome.org</email>
    </othercredit>
    <copyright>
      
        <year>2013;</year>
      
      <holder>Fran Dieguez</holder>
    </copyright>
  </articleinfo>

  <sect1 id="preamble" label="none">
    <title>Preámbulo</title>
    
    <para>A maioría das licenzas de software están concibidas para privar o usuario da súa liberdade de compartir e modificar ese software.  Pola contra, a licenza pública xeral de GNU busca garantir a súa liberdade para compartir e modificar software libre, para se asegurar de que o software é libre para todos os seus usuarios.  Esta licenza pública xeral é de aplicación á maioría do software da Free Software Foundation, así como a calquera outro programa cuxos autores se comprometan a utilizala (hai outro software da Free Software Foundation que se rexe, no seu lugar, pola licenza pública xeral reducida de GNU). Tamén pode aplicala aos seus propios programas.</para>

    <para>Cando se fala de software libre fálase de liberdade, non de prezo.  As nosas licenzas públicas xerais están deseñadas co obxectivo de asegurar a súa liberdade para distribuír copias de software libre (e cobrar por este servizo, se así o desexa), que recibe o código fonte ou pode recibilo se así o desexa, que ten a posibilidade de modificar o software ou empregar partes del en novos programas libres e que, ademais, é consciente de que pode facer todo isto.</para>

    <para>Para protexer os seus dereitos, cómpre introducir certas restricións que impidan que calquera outro lle poida negar estes dereitos  ou pedirlle que renuncie a eles.  Estas restricións implican determinadas responsabilidades para vostede, no caso de distribuír copias do software ou modificalo.</para>

    <para>Por exemplo, se distribúe copias dun programa libre, sexa gratis ou non, tenlle que ceder aos receptores todos os dereitos que ten vostede.  Terase que asegurar de que eses receptores tamén reciben ou poden recibir o código fonte e  terá que mostrarlles os termos desta licenza para informalos dos seus dereitos.</para>

    <para>Dous son os pasos para protexer os seus dereitos: <orderedlist numeration="arabic">
	<listitem>
	  <para>pór o software baixo copyright e</para>
	</listitem>
	<listitem>
	  <para>ofrecerlle esta licenza, que o autoriza legalmente para copiar, distribuír e/ou modificar o software.</para>
	</listitem>
      </orderedlist></para>

    <para>Ademais, como medida de protección para todos os autores e para nós mesmos, é importante deixarlles claro a todas as partes que non existe garantía para este software libre.  No caso de que calquera usuario modifique o software e logo o distribúa a un terceiro, os receptores deben saber que o que se lles entrega non é o software orixinal, de xeito que calquera problema introducido pola outra parte non afecte a reputación dos autores orixinais.</para>

    <para>Por último, calquera programa libre está constantemente ameazado polas patentes de software.   A nosa intención é evitar o perigo de que os redistribuidores dun programa libre obteñan licenzas de patente pola súa conta, convertendo así o seu programa en software rexistrado.  Para evitar que isto suceda, deixamos ben claro que calquera patente debe de garantir a cesión da licenza a calquera usuario posible, de maneira que todo o mundo poida usar o programa libremente ou, en caso contrario, non debe de garantir licenza ningunha a ninguén.</para>

    <para>A seguir detállanse os termos e condicións exactos para a copia, distribución e modificación.</para>

  </sect1>

  <sect1 id="terms" label="none">
    <title>TERMOS E CONDICIÓNS PARA A COPIA, DISTRIBUCIÓN E MODIFICACIÓN</title>

    <sect2 id="sect0" label="0">
      <title>Sección 0</title>
      <para>Esta licenza é aplicable a calquera programa ou produto doutro tipo no que figure un aviso inserido polo titular do copyright que especifique que se pode distribuír baixo os termos desta licenza pública xeral.   De aquí en adiante, o termo <quote>Programa</quote> referirase a calquera programa ou produto deste tipo, mentres que o termo <quote>produto baseado no Programa</quote> referirase tanto ao programa coma a calquera produto derivado consonte a lei de propiedade intelectual, é dicir, calquera produto que conteña o programa ou unha parte del, sexa textual ou con modificacións e/ou traducida a outra lingua.  (De aquí en adiante, a tradución inclúese sen límite ningún no termo <quote>modificación</quote>.)  As licenzas están redactadas na forma de cortesía <quote>vostede</quote>.</para>

      <para>Calquera actividade distinta da copia, distribución e modificación non está cuberta por esta licenza, senón que queda fóra do seu ámbito de aplicación.  Non se restrinxe a acción de executar o Programa, e os resultados tirados do Programa só están cubertos se o seu contido constitúe un produto baseado no Programa (sen importar se foi realizado mediante a execución do Programa).   Que isto sexa certo ou non dependerá do que faga o Programa.</para>
    </sect2>

    <sect2 id="sect1" label="1">
      <title>Sección 1</title>
      <para>Pode modificar a súa copia ou copias do Programa, ou calquera parte del, e obter así un produto baseado no Programa, así como copiar e distribuír as devanditas modificacións ou produto de acordo cos termos do apartado 1 anterior, sempre que cumpra tamén con todas as condicións seguintes:</para>
      
      <para>Pode cobrar unha taxa polo acto físico de transferir unha copia e, se vostede quixer, pode tamén ofrecer unha determinada garantía a cambio do pagamento dunha taxa.</para>
    </sect2>

    <sect2 id="sect2" label="2">
      <title>Sección 2</title>
      <para>Pode modificar a súa copia ou copias do Programa, ou calquera parte del, e obter así un produto baseado no Programa, así como copiar e distribuír as devanditas modificacións ou produto de acordo cos termos do <link linkend="sect1">apartado 1</link> anterior, sempre que cumpra tamén con todas as condicións seguintes: <orderedlist numeration="loweralpha">
	  <listitem>
	    <para>Pode modificar a súa copia ou copias do Programa, ou calquera parte del, e obter así un produto baseado no Programa, así como copiar e distribuír as devanditas modificacións ou produto de acordo cos termos do apartado 1 anterior, sempre que cumpra tamén con todas as condicións seguintes:</para>
	  </listitem>
	  <listitem>
	    <para>Debe facer que calquera produto distribuído ou publicado por vostede, que en todo ou en parte conteña ou derive do Programa ou de calquera parte do Programa, obteña unha licenza no seu conxunto sen que isto repercuta en ningún gasto para calquera terceira parte de acordo cos termos desta licenza.</para>
	  </listitem>
	  <listitem>
	    <para>Se o programa modificado adoita ler comandos de forma interactiva ao executalo, debe facer que, cando se execute para tal uso interactivo na forma máis habitual, mostre ou exhiba un aviso no que se inclúan as oportunas informacións relativas á propiedade intelectual do programa e á ausencia de toda garantía (ou, se for o caso, ao feito de que é vostede quen proporciona a garantía), a indicación de que os usuarios poden redistribuír o programa consonte estas condicións e a información necesaria para que o usuario saiba como acceder a unha copia desta licenza. <note>
		<title>Excepción:</title>
		<para>Se o Programa en si mesmo é interactivo pero non adoita mostrar avisos deste tipo, non é necesario que o seu produto baseado no Programa exhiba ningún aviso.</para>
	      </note></para>
	  </listitem>
	</orderedlist></para>

      <para>Estes requisitos aplícanse ao produto modificado no seu conxunto.  Se o produto contén seccións facilmente identificables non derivadas do Programa, que razoablemente se poidan considerar como produtos separados e independentes por si mesmos, esta licenza e mais os seus termos non serán aplicables ás devanditas seccións cando se distribúan como produtos separados.  Pero se esas mesmas seccións se distribúen como parte dun todo consistente nun produto baseado no Programa, a distribución do todo deberá de se acoller aos termos desta licenza, cuxas disposicións, aplicables a todos os demais titulares da licenza, abranguen o conxunto completo e, en consecuencia, todas e cada unha das partes do conxunto independentemente de quen as escribise.</para>

      <para>Así, o obxectivo deste apartado non é reclamar dereitos ou rebater os seus dereitos sobre calquera produto escrito por vostede na súa totalidade; máis ben ao contrario, o seu obxectivo é exercer o dereito de controlar a distribución de produtos derivados ou colectivos baseados no Programa.</para>

      <para>Ademais, a simple agregación ao Programa (ou a un produto baseado no Programa) doutro produto non baseado no Programa nun volume dun medio de distribución ou almacenamento non implica a inclusión do outro produto no ámbito de aplicación desta licenza.</para>
    </sect2>

    <sect2 id="sect3" label="3">
      <title>Sección 3</title>

      <para>Pode copiar e distribuír o Programa (ou un produto baseado nel, segundo os termos do <link linkend="sect2">apartado 2</link>) en código obxecto ou forma executable de acordo co establecido nos <link linkend="sect1">apartados 1</link> e <link linkend="sect2">2</link> anteriores, sempre que cumpra tamén con un dos seguintes requisitos: <orderedlist numeration="loweralpha">
	  <listitem>
	    <para>Acompañalo do correspondente código fonte completo nun formato lexible por máquina, que se ha de distribuír de acordo co estipulado nos apartados 1 e 2 anteriores nun soporte tipicamente utilizado para o intercambio de software.</para>
	  </listitem>
	  <listitem>
	    <para>Acompañalo dunha oferta por escrito, cunha validez mínima de tres anos, na que se entregue a calquera terceiro interesado, por un custo nunca superior aos gastos nos que vostede poida incorrer pola execución física da distribución do código fonte, unha copia completa en formato lexible por máquina do código fonte correspondente, que se ha de distribuír de acordo co estipulado nos apartados 1 e 2 anteriores nun soporte tipicamente utilizado para o intercambio de software.</para>
	  </listitem>
	  <listitem>
	    <para>Acompañalo da información que recibiu vostede verbo da oferta de distribuír o código fonte correspondente.  (Esta opción só está permitida para a distribución non-comercial, e unicamente no caso de que vostede recibise o programa en código obxecto ou forma executable cunha oferta como a que se acaba de explicar, de acordo co subapartado b anterior.)</para>
	  </listitem>
	</orderedlist></para>

      <para>Enténdese por código fonte dun traballo o seu formato máis idóneo á hora de realizar modificacións nel.  No caso dun traballo executable, enténdese por código fonte completo todo o código fonte para todos os módulos que inclúe, ademais de todos os ficheiros de definición de interfaces asociados e os scripts utilizados para controlar a compilación e a instalación do executable.  Porén, como caso excepcional, o código fonte distribuído non precisa incluír nada que se distribúa normalmente (xa sexa de forma binaria ou fonte) cos compoñentes principais (compilador, núcleo etc.) do sistema operativo en que se executa o executable, agás que ese propio compoñente acompañe o executable.</para>
      
      <para>Se a distribución do código obxecto ou executable se realiza concedendo acceso de copia desde un lugar determinado, entón a concesión de acceso equivalente para copiar o código fonte do mesmo lugar considérase como distribución do código fonte, aínda que ningún terceiro estea obrigado a copiar o código fonte xunto co obxecto.</para>
    </sect2>

    <sect2 id="sect4" label="4">
      <title>Sección 4</title>
      
      <para>Non lle está permitido copiar, modificar, emitir unha sublicenza nin distribuír o Programa agás nos termos especificamente estipulados nesta licenza.  Calquera outro intento de copiar, modificar, emitir unha sublicenza ou distribuír o Programa será considerado nulo, e implicará a cancelación automática dos dereitos que lle concede esta licenza.  No entanto, as partes ás que vostede concedera copias ou dereitos consonte os termos desta licenza non verán resoltas as súas respectivas licenzas, sempre que cumpran plenamente con todo o que nelas se estipula.</para>
    </sect2>

    <sect2 id="sect5" label="5">
      <title>Sección 5</title>

      <para>Dado que aínda non a asinou, non está obrigado a aceptar os termos desta licenza.  Porén, ela é o único que o autoriza a modificar ou distribuír o Programa ou calquera outro produto derivado del.  Estas accións están prohibidas por lei mentres non acepte esta licenza.  Xa que logo, mediante a modificación ou distribución do Programa (ou de calquera produto baseado no Programa) vostede expresa a súa aceptación desta licenza para a realización das devanditas accións, así como de todos os termos e condicións estipulados nela para a copia, distribución ou modificación do Programa ou de calquera produto baseado nel.</para>
    </sect2>

    <sect2 id="sect6" label="6">
      <title>Sección 6</title>

      <para>Cada vez que redistribúa o Programa (ou calquera produto baseado no Programa), o receptor recibirá automaticamente unha licenza por parte do emisor da licenza orixinal que lle permitirá copiar, distribuír ou modificar o Programa consonte estes termos e condicións.  Vostede non poderá impor ningunha outra restrición sobre o exercicio dos dereitos nela estipulados por parte do receptor.  Vostede non é responsable á hora de esixirlle a un terceiro o cumprimento dos termos estipulados nesta licenza.</para>
    </sect2>

    <sect2 id="sect7" label="7">
      <title>Sección 7</title>

      <para>Se, como consecuencia dun proceso xudicial ou dunha acusación por violación de patentes, ou por calquera outra causa (sen restrinxirse aos temas de patentes), resultase a imposición de calquera condición sobre vostede (xa sexa por orde xudicial, por acordo ou por calquera outra causa) que contradiga as condicións desta licenza, iso non o escusa do cumprimento das condicións desta licenza.   De non lle ser posible distribuír o Programa respectando as obrigas contraídas baixo esta licenza e, asemade, calquera outra obriga pertinente, entón terá que deixar de distribuílo en ningún modo.  Por exemplo, no caso de existir unha licenza de patente que non permitise a redistribución do Programa exenta de dereitos de autor por parte de todos aqueles que recibisen copias de maneira directa ou indirecta de vostede, o único xeito de cumprir cos termos da devandita licenza de patente e mais desta licenza ao mesmo tempo sería evitando por completo a distribución do Programa.</para>

      <para>No caso de que calquera sección deste apartado se considerase non válida ou non executable baixo calquera circunstancia concreta será de aplicación o resto do apartado e, en calquera outras circunstancias, aplicarase o apartado no seu conxunto.</para>

      <para>O obxectivo deste apartado non é inducilo a infrinxir ningunha reivindicación de patente nin de calquera outro dereito de propiedade intelectual, así como tampouco impugnar a validez deste tipo de reivindicacións. O único que pretende este apartado é protexer a integridade do sistema de distribución do software libre, levado á práctica mediante o desenvolvemento de licenzas públicas. Moitas persoas contribuíron xenerosamente ao desenvolvemento do amplo abano de software distribuído a través deste sistema, coa confianza de que se aplicará consistentemente. Só o autor/doador pode decidir se está disposto a distribuír o seu software mediante calquera outro sistema; o titular dunha licenza non pode impor este tipo de elección.</para>

      <para>Este apartado pretende deixar ben claro o que considera que é consecuencia do resto desta licenza.</para>
    </sect2>

    <sect2 id="sect8" label="8">
      <title>Sección 8</title>

      <para>No caso de que nalgúns países se restrinxa a distribución e/ou o uso do Programa pola execución dalgunha patente ou interface protexida por copyright, o titular orixinal do copyright que somete o Programa aos termos desta licenza poderá engadir unha limitación explícita á distribución xeográfica que exclúa aqueles países, de xeito que só se permita a distribución nos ou entre os países non suxeitos á devandita exclusión.  Se este for o caso, a licenza incorporaría a limitación citada nos mesmos termos que se fose redactada como parte do corpo desta licenza.</para>
    </sect2>

    <sect2 id="sect9" label="9">
      <title>Sección 9</title>
      
      <para>A Free Software Foundation poderá publicar versións revisadas e/ou novas desta licenza pública xeral no momento en que o estime oportuno.  As novas versións manterán o mesmo espírito ca a actual, se ben poden diferir nalgúns detalles do contido co obxectivo de facer fronte a novos problemas ou preocupacións.</para>

      <para>Cada versión leva un número identificativo diferente.  Se o Programa especifica que lle é aplicable unha versión específica desta licenza e "calquera versión posterior", vostede poderá escoller entre cinguirse aos termos e condicións ben da primeira versión ou ben de calquera versión posterior publicada pola Free Software Foundation.  Se o Programa non especifica ningún número de versión desta licenza, vostede terá a opción de escoller calquera versión de entre todas as publicadas pola Free Software Foundation.</para>
    </sect2>

    <sect2 id="sect10" label="10">
      <title>Sección 10</title>

      <para>Se desexa incluír partes do Programa noutros programas libres cunhas condicións de distribución distintas, deberá de se dirixir ao autor por escrito para lle pedir a súa autorización.  No caso do software co copyright da Free Software Foundation, diríxase por escrito á Free Software Foundation, que en ocasións fai excepcións nestes casos.  A decisión que tomemos basearase nos nosos dous obxectivos de preservar o estatus libre de calquera produto derivado do noso software libre e de promover o compartimento e a reutilización do software en xeral.</para>
    </sect2>

    <sect2 id="sect11" label="11">
      <title>AUSENCIA DE GARANTÍA</title>
      <subtitle>Sección 11</subtitle>

      <para>DADO QUE SE TRATA DUN PROGRAMA ACOMPAÑADO DUNHA LICENZA GRATUÍTA, DENTRO DOS LÍMITES PERMITIDOS POLA LEXISLACIÓN APLICABLE CONSIDÉRASE UN PROGRAMA EXENTO DE TODA GARANTÍA.  AGÁS QUE SE ESPECIFIQUE O CONTRARIO POR ESCRITO, OS TITULARES DO COPYRIGHT E/OU OUTRAS PARTES CEDEN O PROGRAMA "TAL CAL" SEN NINGÚN TIPO DE GARANTÍA, NIN EXPLÍCITA NIN IMPLÍCITA, INCLUÍDAS, AÍNDA QUE SEN EXCLUSIVIDADE, AS GARANTÍAS IMPLÍCITAS DE COMERCIABILIDADE E IDONEIDADE PARA UN DETERMINADO FIN.  A TOTALIDADE DOS RISCOS ASOCIADOS Á CALIDADE E AO RENDEMENTO DO PROGRAMA RECAE SOBRE VOSTEDE.  SE O PROGRAMA RESULTASE DEFECTUOSO, VOSTEDE TERÁ QUE ASUMIR OS CUSTOS DE CALQUERA REPARACIÓN, ARRANXO OU EMENDA.</para>
    </sect2>

    <sect2 id="sect12" label="12">
      <title>Sección 12</title>

      <para>EN NINGÚN CASO, AGÁS QUE ASÍ O ESIXA A LEXISLACIÓN APLICABLE OU QUE EXISTA UN ACORDO POR ESCRITO ENTRE AS PARTES, SE LLE ESIXIRÁ RESPONSABILIDADE AO TITULAR DO COPYRIGHT, OU A CALQUERA OUTRA PARTE AUTORIZADA PARA MODIFICAR E/OU REDISTRIBUÍR O PROGRAMA CONSONTE OS TERMOS DESCRITOS ANTERIORMENTE, POLOS DANOS E PERDAS OCASIONADOS, INCLUÍDOS OS DANOS XERAIS, ESPECIAIS, INCIDENTAIS OU DE IMPORTANCIA DE CALQUERA TIPO QUE POIDAN DERIVAR DO USO OU DA INCAPACIDADE DE USO DO PROGRAMA (O QUE INCLÚE, AÍNDA QUE SEN RESTRICIÓN, BEN A PERDA DE DATOS, OU BEN A INTERPRETACIÓN IMPRECISA DE DATOS, OU BEN AS PERDAS OCASIONADAS A VOSTEDE OU A UN TERCEIRO, OU BEN A IMPOSIBILIDADE DE EXECUTAR O PROGRAMA CON CALQUERA OUTRO PROGRAMA), MESMO SE O DEVANDITO TITULAR OU A OUTRA PARTE FOSEN ADVERTIDOS DA POSIBILIDADE DE QUE SE PRODUCISEN OS DANOS DESCRITOS ANTERIORMENTE.</para>
    </sect2>
  </sect1>
</article>
