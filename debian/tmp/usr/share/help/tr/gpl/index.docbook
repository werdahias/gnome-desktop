<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<!--
     The GNU Public License 2 in DocBook
     Markup by Eric Baudais <baudais@okstate.edu>
     Maintained by the GNOME Documentation Project
     http://developer.gnome.org/projects/gdp
     Version: 1.0.1
     Last Modified: Dec 16, 2000
-->
<article id="index" lang="tr">
  <articleinfo>
    <title>GNU Genel Kamu Lisansı</title>    
    <copyright><year>2000</year> <holder>Free Software Foundation, Inc.</holder></copyright>

      <author><surname>Özgür Yazılım Vakfı</surname></author>

      <publisher role="maintainer">
        <publishername>GNOME Belgelendirme Projesi</publishername>
      </publisher>

      <revhistory>
        <revision><revnumber>2</revnumber> <date>1991-06</date></revision>
      </revhistory>

    <legalnotice id="legalnotice">
      <para><address>Free Software Foundation, Inc. 
	  <street>51 Franklin Street, Fifth Floor</street>, 
	  <city>Boston</city>, 
	  <state>MA</state> <postcode>02110-1301</postcode>
	  <country>USA</country>
	</address>.</para>

      <para>Bu lisans belgesinin tam kopyasının herkes tarafından koplayalanması ve dağıtımı serbest olup, değiştirilmesi yasaktır.</para>
    </legalnotice>

    <releaseinfo>Sürüm 2, Haziran 1991</releaseinfo>

    <abstract role="description"><para>Pek çok yazılımın lisansı, bu ürünlerin paylaşımını ve ürünleri değiştirme özgürlüğünü önleyecek şekilde tasarlanmıştır. Buna karşın, GNU Genel Kamu Lisansları bir özgür yazılımı paylaşma ve değiştirme özgürlüğünü güvence altına alırlar; böylece tüm kullanıcılar için özgür yazılım (free software*) özelliğini koruduğunu garanti altına alır.</para></abstract>
  </articleinfo>

  <sect1 id="preamble" label="none">
    <title>Giriş</title>
    
    <para>Pek çok yazılımın lisansı, bu ürünlerin paylaşımını ve ürünleri değiştirme özgürlüğünü önleyecek şekilde tasarlanmıştır. Buna karşın, GNU Genel Kamu Lisansı bir özgür yazılımı paylaşma ve değiştirme özgürlüğünü güvence altına alır; böylece tüm kullanıcılar için özgür yazılım (free software*) özelliğini koruduğunu garanti altına alır. İşbu Genel Kamu Lisansı, Özgür Yazılım Vakfı yazılımlarının birçoğuna ve yaratıcısının kullanmak istediğini belirttiği diğer çalışmalara uygulanmaktadır. Siz de kendi programlarınızda bu lisansı kullanabilirsiniz.</para>

    <para>Özgür yazılım kavramında vurgulanmak istenen bedava olması değil, özgürce kullanılabiliyor olmasıdır. Genel Kamu Lisanslarımız, özgür yazılımın kopyalarını dağıtma (ve isterseniz bu hizmet için ücretlendirme) özgürlüğünüzü; bu yazılımların kaynak kodlarına erişiminizi ya da isterseniz size sunulmasını, yazılımı değiştirebilmenizi ya da parçalarını yeni özgür yazılımlarda kullanmanızı, ve bu eylemleri gerçekleştirebileceğinizden haberdar olmanızı garanti altına almak için tasarlanmıştır.</para>

    <para>Haklarınızı korumak için, sizi bu haklardan mahrum edecek ve bu haklardan vazgeçmenizi isteyecek kişileri engellemek için bazı kısıtlamalar koymak zorundayız. Bu nedenle, yazılımın kopyalarını dağıttığınız ya da yazılımı değiştirdiğinizde bazı sorumluluklar üstlenirsiniz: Diğerlerinin özgürlüğüne saygı göstermekle yükümlüsünüz.</para>

    <para>Örneğin, böyle bir programın kopyalarını ücretli ya da ücretsiz dağıttığınızda, sahip olduğunuz özgürlükleri bir sonraki alıcılara da devretmek zorundasınız. Onların da bu yazılımların kaynak kodlarına erişimini ya da isterlerse kaynak kodların onlara sunulmasını taahhüt etmek zorundasınız. Ayrıca bu şartları haklarını bilmeleri açısından onlara göstermelisiniz.</para>

    <para>Haklarınızı iki adımda koruyoruz: <orderedlist numeration="arabic">
	<listitem>
	  <para>yazılımı telif hakkıyla koruyor, ve</para>
	</listitem>
	<listitem>
	  <para>size bu lisansı sunarak yazılımı çoğaltmanız, dağıtımını yapmanız ve/veya değiştirmeniz için yasal hak tanıyoruz.</para>
	</listitem>
      </orderedlist></para>

    <para>Ayrıca, herbir yazarın ve bizim korunmamız adına, bu özgür yazılımda garanti olmadığının açıkça anlaşılmasını isteriz. Eğer yazılım başkası tarafında değiştirilmiş ve dağıtılmışsa, alıcıların ellerindeki ürünün özgün olmadığını bilmelerini isteriz; böylece diğerleri tarafından oluşturulan bir sorun asıl yazarın saygınlığına yansımayacaktır.</para>

    <para>Son olarak, her özgür program sürekli olarak yazılım patentlerinin tehdidi altındadır. Bir özgür programın yeniden dağıtımcılarının patent lisansları alıp programı mülkiyet haline getireceği tehdidinden kaçınmayı temenni ediyoruz. Bu durumdan kaçınmak için, her tür patentin herkesin özgür kullanımına göre lisanslanması ya da hiç lisanslanmaması konusunu açıkça belirtiyoruz.</para>

    <para>Çoğaltma, dağıtma ve değiştirme durumları için kesin hükümler ve koşullar aşağıda belirtilmiştir.</para>

  </sect1>

  <sect1 id="terms" label="none">
    <title>KOPYALAMAK; DAĞITMAK VE DEĞİŞTİRMEK İÇİN HÜKÜMLER VE KOŞULLAR</title>

    <sect2 id="sect0" label="0">
      <title>Bölüm 0</title>
      <para>İşbu lisans, telif hakkı sahibi tarafından bu Genel Kamu Lisansı hükümleri altında dağıtılabileceğini belirten bir uyarı içeren her türlü program ve diğer çalışmalara uygulanır. Aşağıda bahsi geçen <quote>Program</quote>, bu tür program ya da çalışmayı kastetmekte olup, <quote>programa dayalı çalışma</quote> telif hakkı yasası altındaki program ya da türevi çalışma anlamına gelir: Başka bir deyişle, programın ya da bir parşasının tam kopyasını ya da değişiklikler ve/veya başka dillere çevrilmiş halini de içeren çalışmadır. (Bundan sonra çeviri, kısıtlama olmaksızın <quote>değişiklik</quote> terimine dahil edilecektir) Her bir lisans sahibi <quote>siz</quote> şeklinde anılacaktır.</para>

      <para>Kopyalama, dağıtma ya da değiştirme dışındaki eylemler işbu lisansa dahil değildir; kapsamın dışında kalır. Bir programı çalıştırma eylemi kısıtlanmamıştır, ve programın çıktısı ancak içeriğiyle programa dayalı bir çalışma oluşturuyorsa dahil edilir. (programı çalıştırmayla oluşturuluşundan bağımsız olarak) Bunun doğruluğu programın ne yaptığına bağlı olarak değişir.</para>
    </sect2>

    <sect2 id="sect1" label="1">
      <title>Bölüm 1</title>
      <para>Her kopyada uygun bir telif hakkı uyarısı ve hakkından vazgeçenleri açıkça ve gereğine uygun yayımlamanız, işbu lisanstan bahseden tüm uyarıları ve bir teminat bulunmadığına dair tüm uyarıları aynen korumanız ve tüm alıcılara programla birlikte bu lisansın bir kopyasını verdiğiniz sürece, programın kaynak kodunun birebir kopyalarını devraldığınız haliyle her ortamda kopyalayabilir ve dağıtabilirsiniz.</para>
      
      <para>Fiziksel olarak taşıdığınız kopyalara ücretlendirme uygulayabilir ya da bu kopyaları ücretsiz dağıtabilirsiniz; isterseniz ücret karşılığında teminat güvencesi sunabilirsiniz.</para>
    </sect2>

    <sect2 id="sect2" label="2">
      <title>Bölüm 2</title>
      <para>Programın kopya ya da kopyalarını ya da bir kısmını, dolayısıyla program baz alınarak oluşturulmuş ürünleri değiştirebilir; aşağıdaki koşulları yerine getirdiğiniz takdirde <link linkend="sect1">Bölüm 1</link>′deki hükümlere bağlı olarak bu gibi değişiklikleri veya çalışmayı kopyalayabilir veya dağıtabilirsiniz:</para>

      <para>Bu gereklilikler değiştirilmiş çalışmanın tümünü kapsar. Eğer çalışmanın tanımlanabilir kısımları programdan türememişse, ve mantık çerçevesinde bağımsız ve ayrı işler olarak nitelendirilebiliyorlarsa, işbu lisans ve hükümleri, ayrı çalışmalar olarak dağıttığınızda bu kısımlara uygulanmaz. Ancak aynı kısımları programa dayalı bir çalışma olan bir bütün olarak dağıtırsanız, bu bütünün dağıtımı tamamen, diğer lisans sahiplerinin tamamına kadar uzanan ve dolayısıyla kimin yazdığına bakılmaksızın her bir tarafa izin sağlayan işbu lisans hükümlerinde yer almalıdır.</para>

      <para>Dolayısıyla, bu bölümün amacı tamamen sizin tarafınızdan yazılan bir çalışmanın hakları üzerinde hak idda etme ya da bu haklarla rekabet etmeyi değil; aksine programa bağlı türev ya da ortak çalışmaların dağıtım veya kontrolünü sağlama hakkını sağlamaktır.</para>

      <para>Buna ek olarak, programa bağlı olmayan başka bir çalışmanın programla (veya programa bağlı bir çalışmayla)bir depolama ya da dağıtım ortamı biriminde çok da önemli olmayan bir birleşimi bu diğer çalışmayı işbu lisans kapsamına sokmaz.</para>
    </sect2>

    <sect2 id="sect3" label="3">
      <title>Bölüm 3</title>

      <para>Programı (ya da <link linkend="sect2">Bölüm 2</link>′de belirtildiği gibi bu programa bağlı çalışmayı) yukarıda geçen <link linkend="sect1">Bölüm 1</link> ve <link linkend="sect2">2</link>′nin hükümlerine bağlı olarak aşağıdakileri sağladığınız takdirde nesne kodu biçiminde ya da çalıştırılabilir biçimde kopyalayabilir veya dağıtabilirsiniz: <orderedlist numeration="loweralpha">
	  <listitem>
	    <para>Yazılım aktarmak için kullanılan bir ortamda yukarıdaki <link linkend="sect1">Bölüm 1</link> ve <link linkend="sect2">2</link>′nin hükümlerine uygun olarak dağıtılması gereken, makine tarafından okunabilen ilgili kaynak kodun tamamını yanına ekleyin; veya,</para>
	  </listitem>
	  <listitem>
	    <para>Yazılım aktarmak için kullanılan bir ortamda yukarıdaki kısım 1 ve 2′nin hükümlerine uygun olarak dağıtılan ve makine tarafından okunabilen ilgili kaynak kodun tamamının bir kopyasını herhangi bir üçüncü tarafa vermek amacıyla, kaynağı fiziksel olarak taşımak için gereken maliyeti aşmayacak şekilde ücretlendirerek, en az üç sene geçerli bir yazılı teklif ekleyin; veya,</para>
	  </listitem>
	  <listitem>
	    <para>İlgili kaynak kodu dağıtmayı teklif etmek konusunda aldığınız bilgiyi ekleyin. (Bu seçeneği, sadece ticari olmayan dağıtımlarda veya programı nesne kodu olarak aldığınız ya da böyle bir teklifle çalıştırılabilir biçimde aldığınız koşullarda yukarıdaki b altbölümüne uygun olarak kullanmanıza izin verilir.)</para>
	  </listitem>
	</orderedlist></para>

      <para>Bir çalışmanın kaynak kodu, o çalışmanın üzerinde değişiklik yapılması tercih edilen biçimidir. Çalıştırılabilir bir çalışma için, kaynak kodun tamamı; içerdiği tüm modüllerin kaynak kodları ve buna ek olarak ilgili arayüz tanım dosyaları, çalıştırılabilir programın derlemesi ve kurulumunu denetlemek için kullanılan betiklerin tümüdür. Ancak, özel bir istisna olarak, dağıtılan kaynak kodun, çalıştırılabilir programın üzerinde çalıştığı işletim sisteminin temel bileşenleriyle (derleyici, çekirdek ve benzeri) normalde dağıtılan (kaynak ya da ikili değer biçiminde) bir şey içermesine, bu bileşen çalıştırılabilen programla birlikte gelmiyorsa, gerek yoktur.</para>
      
      <para>Eğer çalıştırılabilen program veya nesne kodunun dağıtımı belirlenen bir yerden kopyalanmaya erişim sunularak gerçekleştiyse, aynı yerden kaynak kodu kopyalamak için aynı erişimi sunmak, her ne kadar üçüncü taraflar bu nesne koduyla birlikte kaynağı kopyalamaya mecbur bırakılmasa da, kaynak kodun dağıtımı olarak sayılır.</para>
    </sect2>

    <sect2 id="sect4" label="4">
      <title>Bölüm 4</title>
      
      <para>İşbu lisansta özellikle belirtilmediği sürece bir programı kopyalayamaz, değiştiremez, altlisanslayamaz veya yayamazsınız. Aksine bir kopyalama, değiştirme, altlisanslama veya yayma girişimi lisansı feshetmek olup, böyle bir girişim işbu lisans kapsamındaki tüm haklarınızı kendiliğinden sonlandıracaktır. Ancak, işbu lisansın hükümlerine uygun davrandıkları sürece, sizden kopyaları veya hakları işbu lisans altında devralan kişilerin lisanslarına son verilmez.</para>
    </sect2>

    <sect2 id="sect5" label="5">
      <title>Bölüm 5</title>

      <para>İşbu lisansı imzalamadığınız sürece kabul etmek zorunda değilsiniz. Ancak, başka hiçbir şey size programı ya da türevlerini değiştirme veya dağıtma izni vermez. İşbu lisansı kabul etmediğiniz takdirde bu eylemleri gerçekleştirmeniz kanun tarafından yasaklanmıştır. Buna bağlı olarak, programı (ya da programa dayalı bir çalışmayı) değiştirerek veya dağıtarak işbu lisansı ve dolayısıyla programı veya ona bağlı çalışmaları kopyalama, dağıtma ya da değiştirmeye ilişkin işbu lisansın hüküm ve koşullarını kabul ettiğinizi beyan etmiş olursunuz.</para>
    </sect2>

    <sect2 id="sect6" label="6">
      <title>Bölüm 6</title>

      <para>Programı (ya da programa dayalı bir çalışmayı) her seferinde yeniden dağıttığınızda, işbu lisansın hüküm ve koşulları altındaki bir programı kopyalama, dağıtma veya değiştirme izni asıl telif sahiplerinden alıcılara kendiliğinden aktarılmış olur. Burada verilen hakların kullanılmasıyla ilgili kullanıcılara kısıtlamalar dayatamazsınız. İşbu lisans uyarınca uyumluluğun üçüncü taraflarca dayatılmasından yükümlü değilsiniz.</para>
    </sect2>

    <sect2 id="sect7" label="7">
      <title>Bölüm 7</title>

      <para>Eğer bir mahkeme kararı ya da bir patent ihlali suçlaması ya da bir başka sebep (patent konularında kısıtlama olmaksızın) sonucu size dayatılan (mahkeme kararıyla, sözleşmeyle ya da başka bir yolla) şartlar işbu lisansın şartlarıyla çelişiyorsa, bu durum sizi işbu lisansın şartlarından muaf tutmaz. Eğer bu lisansla gelen zorunlulukları ve geçerli diğer zorunlulukları aynı zamanda yerine getirerek bir programı dağıtamıyorsanız, bunun bir sonucu olarak hiçbir zaman dağıtamazsınız. Örneğin, eğer bir patent lisansı programın onu sizden doğrudan ya da dolaylı alan kişiler tarafından telif ücreti olmaksızın yeniden dağıtmasına izin vermiyorsa, bahsi geçen şartları ve işbu lisansın şartlarını yerine getirmek için tek yol programı dağıtmaktan tamamen kaçınmaktır.</para>

      <para>Eğer bu bölümün herhangi bir parçası, özel koşullar altında geçersiz veya uygulanamaz sayılırsa, bu bölümün kalan kısmının uygulanması planlanmış olup, bölümün diğer koşullarda bir bütün olarak uygulanması düşünülmüştür.</para>

      <para>İşbu lisansın amacı sizi herhangi patenti, bir mülkiyet hakkı istemini ihlal etmeye veya bu istemlerin herhangi birinin geçerliliğine itiraz etmeye teşvik etmek değildir; işbu bölüm sadece kamu lisans uygulamaları tarafından uygulanan özgür yazılım dağıtımı sisteminin bütünlüğünü koruma amacı taşır. Pekçok kişi sistemin ilgili uygulamasını esas alarak bu sistem yoluyla çok sayıda yazılım dağıtımına bolca katkı sağladı; başka bir sistem yoluyla yazılımı dağıtıp dağıtmayacağına ve lisans sahibinin böyle bir seçim yapamayacağına karar vermek yazara/bağışçıya kalmış bir karardır.</para>

      <para>Bu bölüm işbu lisansın geri kalan kısmının sonucunun ne olacağına dair inanışı bütün yönleriyle açıkça anlatmak amacını taşır.</para>
    </sect2>

    <sect2 id="sect8" label="8">
      <title>Bölüm 8</title>

      <para>Eğer programın dağıtımı ve/veya kullanımı bazı ülkelerde gerek patentler gerekse telif hakkı alınmış arayüzler tarafından kısıtlanmışsa, programı işbu lisans kapsamına sokan asıl telif hakkı sahibi bahsi geçen ülkeleri hariç tutarak belirli coğrafyada dağıtım kısıtlaması ekleyebilir; böylece bahsedilen şekilde hariç tutulan ülkeler hariç diğer ülkelerde ya da onlar arasında dağıtıma izin verilir. Böyle durumlarda, işbu lisans, kısıtlamaları lisans metninin gövdesinde yazılıymış gibi birleştirir.</para>
    </sect2>

    <sect2 id="sect9" label="9">
      <title>Bölüm 9</title>
      
      <para>Özgür Yazılım Vakfı, Genel Kamu Lisansı’nın değiştirilmiş ve/veya yeni sürümlerini zaman zaman yayımlayabilir. Bu gibi yeni sürümler, halihazırdaki sürümle aynı ruhu taşımakta olup, yeni sorun ve endişeler konusunda ayrıntılandırılması bakımından farklılık gösterebilir.</para>

      <para>Her sürüme ayırt edici bir sürüm numarası verilir. Eğer program işbu lisansın kendisine ve <quote>daha sonraki herhangi bir sürüme</quote> uygulanan bir sürüm numarası belirtiyorsa, o sürümün ya da Özgür Yazılım Vakfı tarafından yayımlanan daha sonraki bir sürümün hüküm ve şartlarına uyma seçeneğine sahipsiniz. Eğer program işbu lisansın herhangi bir sürümünün numarasını özellikle belirtmiyorsa, Özgür Yazılım Vakfı tarafından yayımlanan herhangi bir sürümü seçebilirsiniz.</para>
    </sect2>

    <sect2 id="sect10" label="10">
      <title>Bölüm 10</title>

      <para>Eğer programın bazı parçlarını dağıtım koşulları farklı diğer özgür programlarla birleştirmek istiyorsanız, bu konuda izin almak üzere yazardan izin isteyin. Özgür Yazılım Vakfı tarafından telif hakkı verilen bir yazılım için, Özgür Yazılım Vakfı’na yazın; zaman zaman bu gibi durumlarda istisna yapıyoruz. Kararımız, özgür yazılımımızın bütün türevlerinin özgür olma durumunu korumak ve genel anlamda yazılımın paylaşılması ve yeniden kullanılmasına teşvik etmek gibi iki hedef çerçevesinde şekillenecektir.</para>
    </sect2>

    <sect2 id="sect11" label="11">
      <title>TEMİNAT YOKTUR</title>
      <subtitle>Bölüm 11</subtitle>

      <para>PROGRAM BEDELSİZ OLARAK LİSANSLANDIĞI İÇİN, YÜRÜRLÜKTEKİ YASALARDA İZİN VERİLDİĞİ ÖLÇÜDE PROGRAMIN BİR TEMİNATI YOKTUR. YAZILI OLARAK AKSİ BELİRTİLMEDİĞİ SÜRECE TELİF HAKKI SAHİPLERİ VE/VEYA PROGRAMI YUKARIDA BELİRTİLDİĞİ GİBİ DEĞİŞTİREBİLEN VE/VEYA YENİDEN DAĞITABİLEN DİĞER TARAFLAR, DOĞRUDAN YA DA DOLAYLI OLARAK, TİCARETİNİN YAPILABİLİRLİĞİNE VE ÖZEL BİR AMAÇ İÇİN UYGUNLUĞUNA TEMİNAT VERMEK DE DAHİL, ANCAK BUNUNLA KISITLI KALMAYARAK, HİÇBİR TEMİNAT SUNMADAN PROGRAMI <quote>OLDUĞU GİBİ</quote> TEDARİK EDERLER. PROGRAMIN KALİTESİ VE PERFORMANSIYLA İLGİLİ TÜM RİSK SİZE AİTTİR. PROGRAMIN HATALI OLDUĞU ORTAYA ÇIKARSA, TÜM GEREKLİ HİZMET, ONARIM VE DÜZELTME ÜCRETİ SİZİN TARAFINIZDAN ÜSTLENİLİR.</para>
    </sect2>

    <sect2 id="sect12" label="12">
      <title>Bölüm 12</title>

      <para>YÜRÜRLÜKTEKİ YASALARLA ZORUNLU TUTULMADIĞI YA DA YAZILI OLARAK KARARLAŞTIRILMADIĞI SÜRECE HİÇBİR DURUMDA, HİÇBİR TELİF HAKKI SAHİBİ YA DA YUKARIDA BELİRTİLDİĞİ GİBİ PROGRAMDA DEĞİŞİKLİK YAPABLEN VE/VEYA PROGRAMI TAŞIYABİLEN BAŞKA BİR TARAF; HERHANGİ BİR GENEL, ÖZEL, KAZARA YA DA DOLAYLI OLARAK, PROGRAMIN KULLANIMINDAN VEYA YETERSİZLİĞİNDEN DOĞAN (VERİ KAYBI, VEYA HATALI BULUNAN VERİ, VEYA TARAFINIZDAN YAHUT ÜÇÜNCÜ TARAFLARCA ZARARA UĞRATILMA, VEYA PROGRAMIN DİĞER PROGRAMLARLA ÇALIŞMAKTA BAŞARISIZ OLMASI DA DAHİL OLMAK ÜZERE VE BUNLARLA KISITLI OLMADAN) ZARARLAR DAHİL OLMAK ÜZERE, BÖYLE BİR TELİF SAHİBİ YA DA DİĞER TARAFIN BU GİBİ ZARARLARDAN HABERDAR EDİLDİĞİ KOŞULLARDA BİLE, HASARLAR KONUSUNDA SİZE KARŞI SORUMLU DEĞİLDİR.</para>
    </sect2>
  </sect1>
</article>
