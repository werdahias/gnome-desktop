<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<!-- 
     The GNU Public License 2 in DocBook
     Markup by Eric Baudais <baudais@okstate.edu>
     Maintained by the GNOME Documentation Project
     http://developer.gnome.org/projects/gdp
     Version: 1.0.1
     Last Modified: Feb. 5, 2001
-->
<article id="index" lang="ca">
    <articleinfo>
      <title>Llicència Pública General Reduïda de GNU</title>
      <copyright><year>2000</year> <holder>Free Software Foundation, Inc.</holder></copyright>
      <author><surname>Free Software Foundation</surname></author>
      <publisher role="maintainer">
        <publishername>Projecte de documentació del GNOME</publishername>
      </publisher>
      <revhistory>
        <revision><revnumber>2.1</revnumber> <date>1999-02</date></revision>
      </revhistory>
      <legalnotice id="gpl-legalnotice">
	<para><address>Free Software Foundation, Inc. 
	    <street>51 Franklin Street, Fifth Floor</street>, 
	    <city>Boston</city>, 
	    <state>MA</state> <postcode>02110-1301</postcode>
	    <country>USA</country>
	  </address>.</para>
	<para>Tothom pot copiar i distribuir còpies literals d'aquest document de llicència, però no es permet de fer-hi modificacions.</para>
      </legalnotice>
      <releaseinfo>Versió 2.1, febrer de 1999</releaseinfo>
    <abstract role="description"><para>Les llicències de la majoria de programari estan dissenyades per a prendre-us la llibertat de compartir-lo i modificar-lo. Contràriament, les Llicències públiques generals de GNU estan pensades per a garantir-vos la llibertat de compartir i modificar el programari lliure, per tal d'assegurar que el programari sigui lliure per a tots els seus usuaris i usuàries.</para></abstract>

  
    <othercredit class="translator">
      <personname>
        <firstname>Gil Forcada</firstname>
      </personname>
      <email>gilforcada@guifi.net</email>
    </othercredit>
    <copyright>
      
        <year>2013</year>
      
      <holder>Gil Forcada</holder>
    </copyright>
  
    <othercredit class="translator">
      <personname>
        <firstname>Miquel-Àngel Burgos i Fradeja</firstname>
      </personname>
      <email>miquel.angel.burgos@gmail.com</email>
    </othercredit>
    <copyright>
      
        <year>2020</year>
      
      <holder>Miquel-Àngel Burgos i Fradeja</holder>
    </copyright>
  </articleinfo>

  <sect1 id="preamble" label="none">
    <title>Preàmbul</title>
    
    <para>Les llicències de la majoria de programari estan dissenyades per a prendre-us la llibertat de compartir-lo i modificar-lo. Contràriament, les Llicències públiques generals de GNU estan pensades per a garantir-vos la llibertat de compartir i modificar el programari lliure, per tal d'assegurar que el programari sigui lliure per a tots els seus usuaris i usuàries.</para>

    <para>Quan parlem de programari lliure, ens referim a la llibertat, no al preu. Les nostres llicències públiques generals estan pensades per a assegurar que tingueu la llibertat de distribuir còpies del programari lliure (i de cobrar per aquest servei, si voleu); que rebeu el codi font o que el pugueu rebre si el voleu; que pugueu modificar el programari o fer-ne servir parts en programes lliures nous; i que sapigueu que podeu fer totes aquestes coses.</para>

    <para>Aquesta llicència, la Llicència Pública General Menor, s'aplica a alguns paquets de programari especialment designats --típicament biblioteques-- de la Free Software Foundation i altres autors que decideixen utilitzar-la. Vós també podeu utilitzar-la, però us suggerim que primer penseu detingudament si aquesta llicència o la llicència pública general ordinària són la millor estratègia a utilitzar en qualsevol cas concret, tenint en compte les explicacions següents.</para>

    <para>Quan parlem de programari lliure ens referim a la llibertat, no al preu. Les nostres llicències públiques generals estan pensades per a assegurar que tingueu la llibertat de distribuir còpies del programari lliure (i de cobrar per aquest servei, si voleu); que rebeu el codi font o que, si el voleu, el pugueu rebre; que pugueu modificar el programari o fer-ne servir parts en programes lliures nous; i que sapigueu que podeu fer totes aquestes coses.</para>

    <para>Per a protegir els vostres drets, cal que apliquem restriccions que prohibeixin a tothom de negar-vos aquests drets o demanar-vos que hi renuncieu. Aquestes restriccions suposen determinades responsabilitats per a vós si distribuïu còpies del programari o si el modifiqueu.</para>

    <para>Per exemple, si distribuïu còpies de la biblioteca, gratuïtament o no, heu de donar als beneficiaris tots els drets que us hem atorgat. Heu d'assegurar-vos que també rebin o puguin obtenir el codi font. Si enllaceu un altre codi a la biblioteca, heu de proporcionar fitxers d'objecte complets als destinataris, de manera que puguin tornar a revincular-los amb la biblioteca després de fer canvis a la biblioteca i recompilar-los. I heu de mostrar-los aquestes condicions perquè coneguin els seus drets.</para>

    <para>Protegim els vostres drets amb un mètode de dues fases: (1) tenim el copyright de la biblioteca, i (2) us oferim aquesta llicència, que us dona permís legal per a copiar, distribuir i/o modificar la biblioteca.</para>

    <para>Per a protegir cada distribuïdor, volem deixar molt clar que no hi ha cap garantia per a la biblioteca lliure. A més, si la biblioteca és modificada per una altra persona i transmesa, els receptors haurien de saber que el que tenen no és la versió original, de manera que la reputació de l'autor original no es veurà afectada per problemes que podrien introduir altres persones.</para>

    <para>Finalment, les patents de programari suposen una amenaça constant per a l'existència de qualsevol programa lliure. Volem assegurar-nos que una empresa no pugui restringir de manera efectiva els usuaris d'un programa lliure obtenint una llicència restrictiva d'un titular de patent. Per tant, insistim que qualsevol llicència de patent obtinguda per a una versió de la biblioteca ha de ser coherent amb la plena llibertat d'ús especificada en aquesta llicència.</para>

    <para>La majoria del programari GNU, incloses algunes biblioteques, està cobert per la Llicència Pública General GNU. Aquesta llicència, la Llicència Pública General Reduïda de GNU, s'aplica a certes biblioteques designades, i és molt diferent de la Llicència Pública General ordinària. Utilitzem aquesta llicència amb certes biblioteques per a permetre la vinculació d'aquestes biblioteques amb programes no lliures.</para>

    <para>Quan un programa està enllaçat amb una biblioteca, ja sigui estàticament o mitjançant una biblioteca compartida, la combinació dels dos és legalment una obra combinada, un derivat de la biblioteca original. La Llicència Pública General ordinària, per tant, permet aquesta vinculació només si tota la combinació se n'ajusta als criteris de llibertat. La Llicència Pública General Reduïda permet criteris més laxos per a enllaçar un altre codi amb la biblioteca.</para>

    <para>Anomenem aquesta llicència Llicència Pública General <quote>Reduïda</quote> perquè fa menys per a protegir la llibertat de l'usuari que la Llicència Pública General ordinària. També proporciona a altres desenvolupadors de programari lliure menys avantatge sobre els programes no lliures de la competència. Aquests desavantatges són la raó per la qual utilitzem la Llicència Pública General ordinària en moltes biblioteques. No obstant això, la llicència reduïda proporciona avantatges en determinades circumstàncies especials.</para>

    <para>Per exemple, en rares ocasions, pot haver-hi una necessitat especial de fomentar l'ús més ampli possible d'una determinada biblioteca, de manera que es converteixi en un estàndard de facto. Per a aconseguir-ho, s'ha de permetre que els programes no lliures utilitzin la biblioteca. Un cas més freqüent és que una biblioteca lliure faci la mateixa feina que les biblioteques no lliures més populars. En aquest cas, hi ha poc a guanyar limitant la biblioteca lliure al programari lliure, així que utilitzem la Llicència Pública General Reduïda.</para>

    <para>En altres casos, el permís per a utilitzar una biblioteca particular en programes no lliures permet a un major nombre de persones utilitzar una gran quantitat de programari lliure. Per exemple, el permís per a utilitzar la biblioteca GNU C en programes no lliures permet a moltes més persones utilitzar tot el sistema operatiu GNU, així com la seva variant, el sistema operatiu GNU/Linux.</para>

    <para>Tot i que la Llicència Pública General Menor és menys protectora de la llibertat dels usuaris, garanteix que l'usuari d'un programa que està enllaçat amb la biblioteca tingui la llibertat i els mitjans per a executar aquest programa utilitzant una versió modificada de la biblioteca.</para>

    <para>Els termes i condicions concrets per a copiar, distribuir i modificar s'expliquen a continuació. Pareu atenció a la diferència entre una <quote>obra basada en la biblioteca</quote> i una <quote>obra que utilitza la biblioteca</quote>. La primera conté codi derivat de la biblioteca, mentre que la segona ha de combinar-se amb la biblioteca per a poder executar-se.</para>

  </sect1>

  <sect1 id="terms" label="none">
    <title>TERMES I CONDICIONS PER A LA CÒPIA, DISTRIBUCIÓ I MODIFICACIÓ</title>

    <sect2 id="sect0" label="0">
      <title>Secció 0</title>
      <para>Aquest acord de llicència s'aplica a qualsevol biblioteca de programari o a qualsevol altre programa que contingui un avís col·locat pel titular dels drets d'autor o una altra part autoritzada dient que es pot distribuir sota les condicions d'aquesta Llicència Pública General Reduïda (també anomenada <quote>aquesta llicència</quote>). Ens referim a cada titular de llicència com a <quote>vós</quote>.</para>

      <para>Una <quote>biblioteca</quote> és una col·lecció de funcions de programari i/o dades preparades per a estar convenientment enllaçades amb programes d'aplicació (que utilitzen algunes d'aquestes funcions i dades) per a formar executables.</para>

      <para>La <quote>Biblioteca</quote>, a continuació, es refereix a qualsevol biblioteca de programari o obra que s'hagi distribuït sota aquests termes. Una <quote>obra basada en la Biblioteca</quote> és o bé la Biblioteca o qualsevol obra derivada sota la llei de copyright: és a dir, una obra que conté la Biblioteca sencera o una part, sigui literal o amb modificacions i/o traduïda directament a un altre idioma. (En endavant, la traducció s'inclou sense limitació en el terme <quote>modificació</quote>.)</para>	

      <para>El <quote>codi font</quote> d'una obra és la forma preferida de l'obra per a fer-hi modificacions. En una biblioteca, el codi font complet és tot el codi font de tots els mòduls que conté, a més de qualsevol fitxer de definició d'interfície associat, més els scripts utilitzats per a controlar la compilació i la instal·lació de la biblioteca.</para>

      <para>Les activitats que no siguin la còpia, la distribució i la modificació no estan cobertes per aquesta llicència; en són fora de l'abast. L'acte d'executar un programa utilitzant la biblioteca no està restringit, i la sortida del programa només està coberta si el contingut en constitueix una obra basada en la biblioteca (independentment de l'ús de la biblioteca en una eina per a escriure-la). Que això sigui cert depèn del que faci la biblioteca i del que faci el programa que utilitza la biblioteca.</para>

    </sect2>

    <sect2 id="sect1" label="1">
      <title>Secció 1</title>
      <para>Podeu copiar i distribuir còpies literals del codi font de la biblioteca tal com el rebeu, en qualsevol mitjà, sempre que publiqueu en cada còpia, de manera adient i ben visible, una nota de copyright i una renúncia de garantia; manteniu intactes tots els avisos que fan referència a aquesta llicència i a l'absència de garanties de cap mena; i distribuïu una còpia d'aquesta llicència juntament amb la biblioteca.</para>
      
      <para>Podeu cobrar un preu per l'acte físic de trametre una còpia i podeu, si així ho voleu, oferir protecció de garantia a canvi d'un preu.</para>
    </sect2>

    <sect2 id="sect2" label="2">
      <title>Secció 2</title>
      <para>Podeu modificar la còpia o còpies de la biblioteca o qualsevol tros de la biblioteca i, així, fer una obra basada en la biblioteca, i podeu copiar i distribuir aquestes modificacions o obres sota els termes de la <link linkend="sect1">Secció 1</link> anterior, sempre que també compliu les  condicions següents: <orderedlist numeration="loweralpha">
	  <listitem>
	    <para>L'obra modificada ha de ser una biblioteca de programari.</para>
	  </listitem>
	  <listitem>
	    <para>Heu de fer que els fitxers modificats portin indicacions ben visibles que diguin que heu modificat els fitxers, i hi heu d'escriure la data de la modificació.</para>
	  </listitem>
	  <listitem>
	    <para>Heu de fer que tota l'obra estigui autoritzada sense cap càrrec a totes les terceres parts en virtut dels termes d'aquesta llicència.</para>
	  </listitem>
	  <listitem>
	    <para>Si una funcionalitat de la biblioteca modificada es refereix a una funció o una taula de dades que ha de subministrar un programa d'aplicació que utilitza la funcionalitat, d'una manera altra que com un argument passat quan s'invoca la funcionalitat, heu de fer un esforç de bona fe per a assegurar que, en el cas que una aplicació no proporcioni aquesta funció o taula, la funcionalitat encara funcioni, i que realitzi tota part del seu propòsit que continuï sent significativa.</para>

	    <para>(Per exemple, una funció d'una biblioteca per a calcular arrels quadrades té un propòsit que és molt ben definit independentment de l'aplicació. Per tant, la subsecció 2d requereix que qualsevol funció o taula subministrada per l'aplicació utilitzada per a aquesta funció ha de ser opcional: si l'aplicació no la proporciona, la funció d'arrel quadrada ha de calcular arrels quadrades igualment.)</para>
	      
	    <para>Aquests requeriments afecten l'obra modificada com un tot. Si hi ha parts identificables que no deriven de la biblioteca, i es poden considerar raonablement com a obres independents en si mateixes, aquesta llicència i els seus termes no s'apliquen a aquelles parts quan les distribuïu com a obres independents. Però quan distribuïu les mateixes seccions com a parts un tot que sigui una obra basada en la biblioteca, la distribució del conjunt s'ha de fer d'acord amb els termes d'aquesta llicència, els permisos a altres titulars de la qual abasten el conjunt sencer i, per tant, totes i cadascuna de les parts, independentment de qui les hagi escrites.</para>

	    <para>Així doncs, la intenció d'aquesta secció no és reclamar o disputar-vos cap dret sobre treballs que heu escrit completament vosaltres mateixos. La intenció és més aviat exercir el dret a controlar la distribució d'obres derivades o col·lectives basades en la biblioteca.</para>

	    <para>A més, la mera agregació d'una altra obra no basada en la Biblioteca a la Biblioteca (o a una obra basada en la Biblioteca) en un volum d'un mitjà d'emmagatzematge o distribució no fa que l'altra obra sigui a l'abast d'aquesta Llicència.</para>
	  </listitem>	      
	</orderedlist></para>

    </sect2>

    <sect2 id="sect3" label="3">
      <title>Secció 3</title>

      <para>Podeu optar per aplicar els termes de la Llicència Pública General GNU ordinària en lloc d'aquesta Llicència a una còpia donada de la Biblioteca. Per a fer-ho, heu d'alterar tots els avisos que fan referència a aquesta llicència, de manera que es refereixin a la Llicència Pública General GNU ordinària, versió 2, en lloc d'aquesta llicència. (Si ha aparegut una versió més nova que la versió 2 de la Llicència Pública General GNU, podeu especificar-la si ho voleu.) No feu cap altre canvi en aquests avisos.</para>

      <para>Una vegada que es fa aquest canvi en una còpia concreta, és irreversible per a la còpia, de manera que la Llicència Pública General GNU ordinària s'aplica a totes les còpies i obres derivades posteriors fetes a partir de la còpia.</para>
      
      <para>Aquesta opció és útil quan voleu copiar part del codi de la biblioteca en un programa que no és una biblioteca.</para>
    </sect2>

    <sect2 id="sect4" label="4">
      <title>Secció 4</title>
      
      <para>Podeu copiar i distribuir la biblioteca (o una porció o derivada, sota la <link linkend="sect2">Secció 2</link>) en el codi d'objecte o en forma executable sota els termes de les Seccions <link linkend="sect1">1</link> i <link linkend="sect2">2</link> anteriors sempre que l'acompanyeu amb el codi font complet corresponent llegible per màquina, que s'ha de distribuir sota els termes de les Seccions <link linkend="sect1">1</link> i <link linkend="sect2">2</link> anteriors en un mitjà habitual d'intercanvi de programari.</para>

      <para>Si la distribució del codi d'objecte es fa donant accés per a copiar des d'un lloc designat, donar un accés equivalent per a copiar el codi font des del mateix lloc satisfà el requisit de distribuir el codi font, tot i que els tercers no estan obligats a copiar la font juntament amb el codi d'objecte.</para>

    </sect2>

    <sect2 id="sect5" label="5">
      <title>Secció 5</title>

      <para>Un programa que no conté cap derivat de cap porció de la biblioteca, però que està dissenyat per a funcionar amb la biblioteca mitjançant la compilació o l'enllaç, s'anomena una <quote>obra que utilitza la biblioteca</quote>. Aquesta obra, aïllada, no és una obra derivada de la biblioteca, i, per tant, queda fora de l'àmbit d'aquesta llicència.</para>

      <para>No obstant això, l'enllaç d'una <quote>obra que utilitza la biblioteca</quote> amb la biblioteca crea un executable que és més aviat un derivat de la biblioteca (perquè conté porcions de la biblioteca) que una <quote>obra que utilitza la biblioteca</quote>. Per tant, l'executable està cobert per aquesta llicència. La <link linkend="sect6">Secció 6</link> estableix els termes de distribució d'aquests executables.</para>

      <para>Quan una <quote>obra que utilitza la biblioteca</quote> utilitza material d'un fitxer de capçalera que forma part de la biblioteca, el codi d'objecte de l'obra pot ser una obra derivada de la biblioteca encara que el codi font no ho sigui. Si això sigui cert és especialment significatiu si l'obra es pot enllaçar sense la biblioteca, o si l'obra és en si mateixa una biblioteca. El llindar perquè això sigui cert no està definit amb precisió per la llei.</para>

      <para>Si un objecte d'aquest tipus només utilitza paràmetres numèrics, disposicions d'estructures de dades i accessoris, i macros petites i funcions en línia petites (deu línies de longitud o menys), l'ús del fitxer objecte no està restringit, independentment de si és legalment una obra derivada. (Els executables que contenen aquest codi d'objecte més porcions de la biblioteca seguiran encaixant en la <link linkend="sect6">Secció 6</link>.)</para>

      <para>En cas contrari, si l'obra és un derivat de la biblioteca, podeu distribuir el codi d'objecte de l'obra sota els termes de la <link linkend="sect6">Secció 6</link>. Qualsevol executable que contingui aquesta obra també encaixa en la <link linkend="sect6">Secció 6</link>, tant si estan vinculats directament amb la biblioteca mateixa com si no.</para>
    </sect2>

    <sect2 id="sect6" label="6">
      <title>Secció 6</title>

      <para>Com a excepció de les seccions anteriors, també podeu combinar o enllaçar una <quote>obra que utilitza la biblioteca</quote> amb la biblioteca per a produir una obra que contingui porcions de la biblioteca, i distribuir aquesta obra sota els termes de la vostra elecció, sempre que els termes permetin la modificació del treball per al propi ús del client i enginyeria inversa per a la depuració d'aquestes modificacions.</para>

      <para>Heu d'avisar clarament amb cada còpia de l'obra que la biblioteca s'hi utilitza i que la biblioteca i el seu ús estan coberts per aquesta llicència. Heu de proporcionar una còpia d'aquesta llicència. Si l'obra mostra avisos de copyright durant l'execució, hi heu d'incloure l'avís de copyright de la biblioteca, així com una referència que dirigeixi l'usuari a la còpia d'aquesta llicència. A més, heu de fer una d'aquestes coses: <orderedlist numeration="loweralpha">
	  <listitem>
	    <para id="sect6a">Acompanyeu l'obra amb el codi font llegible per màquina corresponent complet de la biblioteca incloent-hi els canvis que s'han utilitzat en l'obra (que s'han de distribuir sota les condicions de les seccions <link linkend="sect1">1</link> i <link linkend="sect2">2</link> anteriors); i, si l'obra és un executable enllaçat amb la biblioteca, amb l'<quote>obra que utilitza la biblioteca</quote> llegible per màquina completa, com a codi d'objecte i/o codi font, de manera que l'usuari pugui modificar la biblioteca i revincular per a produir un executable modificat que contingui la biblioteca modificada. (S'entén que l'usuari que canvia el contingut dels fitxers de definicions de la biblioteca no serà necessàriament capaç de recompilar l'aplicació per a utilitzar les definicions modificades.)</para>
	  </listitem>
	  <listitem>
	    <para>Utilitzeu un mecanisme de biblioteca compartida adequat per a enllaçar amb la biblioteca. Un mecanisme adequat és aquell que (1) utilitza en temps d'execució una còpia de la biblioteca ja present en el sistema informàtic de l'usuari, en lloc de copiar les funcions de la biblioteca en l'executable, i (2) funcionarà correctament amb una versió modificada de la biblioteca, si l'usuari n'instal·la una, sempre que la versió modificada sigui compatible amb la versió amb la qual es va fer l'obra.</para>
	  </listitem>
	  <listitem>
	    <para>Acompanyeu l'obra amb una oferta escrita, vàlida almenys durant tres anys, per a donar al mateix usuari els materials especificats a la <link linkend="sect6a">Subsecció 6a</link> anterior, per un cost no superior al cost de realitzar aquesta distribució.</para>
	  </listitem>
	  <listitem>
	    <para>Si la distribució de l'obra es fa donant accés per a copiar des d'un lloc designat, doneu un accés equivalent per a copiar els materials especificats anteriors des del mateix lloc.</para>
	  </listitem>
	  <listitem>
	    <para>Comproveu que l'usuari ja ha rebut una còpia d'aquests materials o que ja li n'heu enviat una còpia.</para>
	  </listitem>
	</orderedlist></para>

	<para>Per a un executable, la forma requerida de l'<quote>obra que utilitza la biblioteca</quote> ha d'incloure qualsevol dada i programes d'utilitat necessaris per a reproduir-ne l'executable. No obstant això, com a excepció especial, els materials a distribuir no han d'incloure res que normalment es distribueixi (ja sigui en forma font o binària) amb els components principals (compilador, nucli, etc.) del sistema operatiu en el qual s'executa l'executable, llevat que el mateix component acompanyi l'executable.</para>

	<para>Pot succeir que aquest requisit contradigui les restriccions de llicència d'altres biblioteques propietàries que normalment no acompanyen el sistema operatiu. Aquesta contradicció significa que no podeu utilitzar-les amb la biblioteca en un executable que distribuïu.</para>

    </sect2>

    <sect2 id="sect7" label="7">
      <title>Secció 7</title>

      <para>Podeu col·locar les funcionalitats de biblioteca que són una obra basada en la biblioteca una al costat de l'altra en una sola biblioteca juntament amb altres funcionalitats de la biblioteca no cobertes per aquesta llicència, i distribuir aquesta biblioteca combinada, sempre que la distribució per separat de l'obra basada en la biblioteca i de les altres funcionalitats de la biblioteca sigui altrament permesa, i sempre que feu aquestes dues coses:</para>

	<orderedlist numeration="loweralpha">
	  <listitem>
	    <para>Acompanyeu la biblioteca combinada d'una còpia de la mateixa obra basada en la biblioteca, sense combinar-la amb cap altra funcionalitat de biblioteca. Això ha de distribuir-se segons els termes de les seccions anteriors.</para>
	</listitem>
	<listitem>
	  <para>Incloeu un avís prominent amb la biblioteca combinada del fet que part n'és una obra basada en la biblioteca, i explicant on trobar la forma no combinada que acompanya la mateixa obra.</para>
	</listitem>
      </orderedlist>	    
    </sect2>

    <sect2 id="sect8" label="8">
      <title>Secció 8</title>

      <para>No podeu copiar, modificar, subllicenciar, enllaçar o distribuir la biblioteca excepte com s'estableix expressament en aquesta llicència. Qualsevol altre intent de copiar, modificar, subllicenciar, enllaçar o distribuir la biblioteca és nul, i automàticament finalitzarà els vostres drets sota aquesta llicència. No obstant això, les parts que hagin rebut còpies, o drets, de vós sota aquesta llicència no veuran finalitzades llurs llicències sempre que aquestes parts es mantinguin en ple compliment.</para>
    </sect2>

    <sect2 id="sect9" label="9">
      <title>Secció 9</title>
      
      <para>No esteu obligat a acceptar aquesta llicència, ja que no l'heu signada. Tanmateix, no hi ha cap altra opció que us doni permís per a modificar o distribuir la biblioteca o les seves obres derivades. Aquestes accions queden prohibides per la llei si no accepteu aquesta llicència. Així doncs, en modificar o distribuir la biblioteca o les seves obres derivades, indiqueu que accepteu aquesta llicència per a fer-ho, i tots els seus termes i condicions per a copiar, distribuir o modificar la biblioteca o obres que hi estiguin basades.</para>
    </sect2>

    <sect2 id="sect10" label="10">
      <title>Secció 10</title>

      <para>Cada vegada que redistribuïu la biblioteca (o qualsevol obra basada en la biblioteca), el destinatari rep automàticament una llicència del llicenciador original per a copiar, distribuir, enllaçar o modificar la biblioteca subjecta a aquests termes i condicions. No podeu imposar cap més restricció a l'exercici dels drets concedits aquí als beneficiaris. No sou responsable de fer complir la llicència a tercers.</para>
    </sect2>

    <sect2 id="sect11" label="11">
      <title>Secció 11</title>

      <para>Si, a conseqüència d'una decisió judicial, una demanda per infracció d'una patent o per qualsevol altra raó (no exclusivament relacionada amb patents), se us imposen condicions (tant si és per ordre judicial, acord, o el que sigui) que contradiuen les condicions d'aquesta llicència, no quedeu excusat de les condicions d'aquesta llicència. Si no us és possible distribuir de manera que satisfeu alhora les obligacions que us imposa aquesta llicència i qualsevol altra obligació pertinent, aleshores no podeu distribuir el programa en absolut. Per exemple, si una llicència de patent no permetés redistribuir gratuïtament la biblioteca a aquells que hagin rebut còpies de vós directament o indirecta, aleshores l'única manera en què podríeu satisfer tant això com aquesta llicència seria abstenir-vos completament de distribuir la biblioteca.</para>

      <para>Si qualsevol fragment d'aquesta secció quedés invalidat o no es pogués fer complir en qualsevol circumstància particular, la intenció és que s'apliqui el balanç de la secció, i que s'apliqui la secció completament en altres circumstàncies.</para>

      <para>El propòsit d'aquesta secció no és induir-vos a infringir cap patent ni cap altre requeriment del dret a la propietat, o a discutir-ne la validesa; l'únic propòsit d'aquesta secció és protegir la integritat del sistema de distribució de programari lliure, que s'implementa amb pràctiques de llicència pública. Molta gent ha fet generoses contribucions a l'ampli ventall de programari distribuït per aquest sistema refiant-se de l'aplicació consistent del sistema; li pertoca a l'autor, autora o donant decidir si vol distribuir programari per algun altre sistema, i un beneficiari de la llicència no pot imposar aquesta elecció.</para>

      <para>Aquesta secció pretén deixar del tot clar el que es considera una conseqüència de la resta de la llicència.</para>
    </sect2>

    <sect2 id="sect12" label="12">
      <title>Secció 12</title>

      <para>Si hi ha països que restringeixen la distribució o l'ús de la biblioteca, ja sigui per patents o per interfícies sota copyright, el posseïdor del copyright original que posi la biblioteca sota aquesta llicència pot afegir limitacions geogràfiques explícites que excloguin aquests països, de manera que la distribució només quedi permesa dins dels països no exclosos, o entre ells. En tal cas, aquesta llicència incorpora la limitació com si estigués escrita en el text de la llicència.</para>
    </sect2>

    <sect2 id="sect13" label="13">
      <title>Secció 13</title>

      <para>La Free Software Foundation pot publicar versions revisades i/o noves de la Llicència Pública General Reduïda de tant en tant. L'esperit d'aquestes noves versions serà similar al de la versió actual, però poden diferir en detalls a l'hora d'abordar nous problemes o preocupacions.</para>

      <para>Cada versió té un número de versió distintiu. Si la biblioteca especifica un número de versió d'aquesta llicència que s'hi aplica i també a <quote>qualsevol versió posterior</quote>, teniu l'opció de seguir els termes i condicions d'aquesta versió o de qualsevol versió posterior publicada per la Free Software Foundation. Si la biblioteca no especifica un número de versió de llicència, podeu triar qualsevol versió publicada per la Free Software Foundation.</para>
    </sect2>

    <sect2 id="sect14" label="14">
      <title>Secció 14</title>

      <para>Si voleu incorporar parts de la biblioteca en altres programes lliures les condicions de distribució dels quals siguin incompatibles amb aquestes, escriviu a l'autor per a demanar permís. Per al programari que és copyright de la Free Software Foundation, escriviu a la Free Software Foundation; de vegades hi fem excepcions. La nostra decisió es guiarà pels dos objectius de preservar l'estatus lliure de tots els derivats del nostre programari lliure i de promoure l'intercanvi i reutilització del programari en general.</para>
    </sect2>

    <sect2 id="sect15" label="15">
      <title>SENSE GARANTIA</title>
      <subtitle>Secció 15</subtitle>

      <para>ATÈS QUE LA BIBLIOTECA TÉ UNA LLICÈNCIA GRATUÏTA, NO SE N'OFEREIX CAP TIPUS DE GARANTIA, EN LA MESURA PERMESA PER LES LLEIS APLICABLES. LLEVAT DELS CASOS EN QUÈ S'AFIRMI EL CONTRARI PER ESCRIT, ELS LLICENCIADORS I/O LES ALTRES PARTS OFEREIXEN LA BIBLIOTECA <quote>TAL COM ÉS</quote>, SENSE CAP TIPUS DE GARANTIA, NI EXPLÍCITA NI IMPLÍCITA; AIXÒ INCLOU, SENSE LIMITAR-S'HI, LES GARANTIES IMPLÍCITES DE COMERCIALITZACIÓ I ADEQUACIÓ A UN ÚS CONCRET. TOT EL RISC PEL QUE FA A LA QUALITAT I RENDIMENT DE LA BIBLIOTECA ÉS VOSTRE. EN CAS QUE LA BIBLIOTECA RESULTÉS DEFECTUOSA, VÓS ASSUMIU TOT EL COST D'ASSISTÈNCIA, REPARACIÓ O CORRECCIÓ.</para>
    </sect2>

    <sect2 id="sect16" label="16">
      <title>Secció 16</title>

      <para>EL POSSEÏDOR DEL COPYRIGHT, O QUALSEVOL ALTRA PART QUE PUGUI MODIFICAR O REDISTRIBUIR LA BIBLIOTECA TAL COM ES PERMET MÉS AMUNT NO US HAURÀ DE RESPONDRE EN CAP CAS, TRET DEL QUE REQUEREIXI LA LLEI APLICABLE O ELS ACORDS PER ESCRIT, PER PERJUDICIS, INCLOSOS ELS INCIDENTALS, DERIVATS, ESPECIALS O GENERALS QUE ES DERIVIN DE L'ÚS O DE LA IMPOSSIBILITAT D'ÚS DE LA BIBLIOTECA (INCLOSES, ENTRE D'ALTRES, LES PÈRDUES DE DADES, LES DADES QUE LA BIBLIOTECA HAGI MALMÈS, LES PÈRDUES QUE US HAGI PROVOCAT A VÓS O A TERCERS O LA IMPOSSIBILITAT QUE LA BIBLIOTECA FUNCIONI AMB QUALSEVOL ALTRE PROGRAMA), FINS I TOT SI AQUEST POSSEÏDOR O ALTRA PART HA ESTAT ADVERTIDA DE LA POSSIBILITAT D'AQUESTS PERJUDICIS.</para>
    </sect2>
  </sect1>
</article>
